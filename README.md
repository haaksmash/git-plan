# git-plan

## Usage
To start,  set the planner up for success by running `git plan init`. This will
do some minimal book-keeping and setup. You should only need to run this one time.

Then, toss out a `git plan new` to get your productivity juices flowing!

```sh
$ git plan new "Make it easy to understand sub-pieces of work in a git repository"
```

this will drop you into your shell's `$EDITOR`, with the following skeleton:

```
# git-plan will ignore any lines that start with a `#`, like this one.
# git-plan parses (and modifies!) this file to understand how
# it should understand future commands.
[ ] Make it easy to understand sub-pieces of work in a git repository
  [ ] <your first sub-task goes here!>
```

You can add lines to this file as you like. `git-plan` expects a markdown-like
list of tasks, with a specific format:

```
[ ] Make it easy to understand sub-pieces of work in a git repository
 ^  ^
 |  a description of what the step should accomplish
 |
 a pair of brackets with either a space or an 'x' character, to indicate
 whether the step is finished or not.
```

Here's an example of a more fleshed-out plan:

```
[ ] Make it easy to understand sub-pieces of work in a git repository
  [ ] Compose a README file that shows how it should work
    [X]  Write a section explaining the motivation.
    [ ] Write the usage section
  [ ] git-plan works
    [ ] parse and tokenize a plan file
  [ ] git-plan lives up to its README
    [ ] git plan <string> can be invoked
    [ ] git plan <string> opens an editor with a starter file
```

`git plan` will create, and switch to, a new branch for your plan.

You can check which plan is being tracked with a simple call to `git plan list`:
```sh
$ git plan list
Here`s all the plans I know about:

*| Make it easy to understand sub-pieces of work in a git repository
 | Take over the world
```

A more friendly, and in-depth, message comes from `git plan`:
```sh
$ git plan
Our currently active plan is:
  Make it easy to understand sub-pieces of work in a git repository

I think we`re on this step:
  Write the usage section
```

When you've completed a step, let `git plan` know; the planner will
automatically find the next step that makes sense for you to focus on:

```sh
$ git plan
Our currently active plan is:
  Make it easy to understand sub-pieces of work in a git repository

I think we`re on this step:
  Write the usage section

$ git plan next
Great work. I`ve committed our progress in 10281af.

Our next step is:
  parse and tokenize a plan file
```

Sometimes you have to make big changes to your plan. Your assistant
is ready and able to help:

```sh
$ git plan refine
```

This will open your `$EDITOR`, loaded with the current plan, for you to
adjust however you need. When you're done, the planner will let you know
what it thinks is next:

```sh
Got it; based on your changes I`ve switched our focus to this:
  A step I didn`t think was needed originally
```

If you really need to shift gears to a new plan, a quick `git-plan switch` will
take care of it for you; simply fuzzy-find the plan description you want and the
planner will switch over to the correct branch automatically.

## Why make this?
I once listened to a podcast whose subject was the dearth of improvement in the
**experience** of programming. `git`, a tool that I use all the time---both
professionally and as a hobbyist---was called out as being a perfect example of
how bad the experience of programming really is.

I think they're right; it's almost comical how obtuse the
command-line-interface that `git` provides is. I'd bet some serious money that
90% of the developers who happily use `git` to manage their projects' histories
are only familiar with four commands:

- `git commit`
- `git checkout`
- `git push`
- `git pull`

and even for these, the majority of developers would not know that they are
'porcelain' around the plumbing of `git-commit`, etc.
