use crate::storage;
use regex::Regex;
use std::path;

#[derive(Debug)]
pub struct PlanLine {
    pub indentation: usize,
    pub completion_code: String,
    pub label: String,
}

#[derive(Clone, Debug)]
pub struct Step {
    pub label: String,
    pub completion_code: String,
    pub children: Vec<Box<Step>>,
}

pub enum StepPrintFormat {
    Pretty,
}
impl Step {
    pub fn load<P: AsRef<path::Path>>(
        git_root: P,
        slug: &String,
        verbosity: u32,
    ) -> Result<Box<Step>, std::io::Error> {
        get_plan_lines(git_root, slug)
            .and_then(|lines| line_to_plan_line(lines))
            .and_then(|plan_lines: Vec<PlanLine>| {
                let plan = construct_plan_tree(&plan_lines[0], &plan_lines[1..]);
                if verbosity > 3 {
                    println!("plan specification: {:?}", plan);
                }
                Ok(plan)
            })
    }

    pub fn store<P: AsRef<path::Path>>(
        &self,
        git_root: P,
        slug: &String,
    ) -> Result<(), std::io::Error> {
        storage::write_plan_file(git_root, slug, self.to_string(StepPrintFormat::Pretty))
    }

    pub fn to_string(&self, format: StepPrintFormat) -> String {
        match format {
            StepPrintFormat::Pretty => {
                let current_step_string = format!(
                    "[{}] {}",
                    if self.completion_code != " " {
                        "X"
                    } else {
                        " "
                    },
                    self.label,
                );
                if self.children.len() == 0 {
                    return current_step_string;
                }
                current_step_string
                    + "\n  "
                    + self
                        .children
                        .iter()
                        .map(|p| {
                            let re = Regex::new(r"\n  ").unwrap();
                            re.replace_all(p.to_string(StepPrintFormat::Pretty).as_str(), "\n    ")
                                .to_string()
                        })
                        .collect::<Vec<String>>()
                        .join("\n  ")
                        .as_str()
            }
        }
    }

    fn get_substep(&mut self, spec: &[usize]) -> &mut Step {
        if spec.len() == 0 {
            return self;
        }
        if let Some((first, rest)) = spec.split_first() {
            if rest.len() != 0 {
                return self.children[first.to_owned()].get_substep(rest);
            } else {
                return &mut *self.children[first.to_owned()];
            }
        }

        panic!("illegal spec: no such step {:?}", spec)
    }

    pub fn best_next_step(&mut self) -> &mut Step {
        let identifier = best_next_step_recursive(self, Vec::new());
        self.get_substep(identifier.as_slice())
    }
}

fn best_next_step_recursive(plan: &Step, mut identifier: Vec<usize>) -> Vec<usize> {
    if plan.children.len() == 0 {
        return identifier;
    }
    let children = plan.children.iter();
    let next_child = children
        .into_iter()
        .enumerate()
        .filter(|(_, c)| c.completion_code == " ")
        .collect::<Vec<(usize, &Box<Step>)>>();
    if next_child.len() == 0 {
        return identifier;
    }
    let (i, subplan) = next_child[0];
    identifier.push(i);
    return best_next_step_recursive(&subplan, identifier.clone());
}

fn construct_plan_tree(root: &PlanLine, remainder: &[PlanLine]) -> Box<Step> {
    let direct_children_indices: Vec<usize> = (0..remainder.len())
        .filter(|&i| remainder[i].indentation == root.indentation + 2)
        .collect();

    let children = if direct_children_indices.len() == 0 {
        Vec::new()
    } else {
        let mut most_children: Vec<Box<Step>> = direct_children_indices
            [0..direct_children_indices.len() - 1]
            .iter()
            .enumerate()
            .map(|(i, &index_to_remainder)| {
                construct_plan_tree(
                    &remainder[index_to_remainder],
                    &remainder[(index_to_remainder + 1)..(direct_children_indices[i + 1])],
                )
            })
            .collect();
        let final_child_tree =
            &remainder[direct_children_indices[direct_children_indices.len() - 1]..];
        most_children.push(construct_plan_tree(
            &final_child_tree[0],
            &final_child_tree[1..],
        ));
        most_children
    };

    Box::new(Step {
        label: root.label.clone(),
        completion_code: root.completion_code.clone(),
        children: children,
    })
}

fn get_plan_lines<P: AsRef<path::Path>>(
    git_root: P,
    slug: &String,
) -> Result<Vec<String>, std::io::Error> {
    storage::read_plan_file(git_root, slug).and_then(|s| Ok(construct_plan_lines_from_string(s)))
}

fn construct_plan_lines_from_string(s: String) -> Vec<String> {
    s.split("\n")
        // honor our promise about ignoring comment lines!
        .filter(|&l| !l.starts_with("#"))
        // empty lines should also be ignored
        .filter(|&l| l.len() > 0)
        .map(String::from)
        .collect()
}

fn line_to_plan_line(lines: Vec<String>) -> Result<Vec<PlanLine>, std::io::Error> {
    let re = Regex::new(r"^(?P<Indent>(  )*)\[(?P<Completion>.+?)\](?: \[(?P<Minutes>\d+) minutes\])? (?P<Label>.*)$").unwrap();
    Ok(lines
        .iter()
        .map(|l| {
            let captures = re.captures(l).expect("no regex match");
            PlanLine {
                indentation: captures
                    .name("Indent")
                    .expect("no indentation")
                    .as_str()
                    .len(),
                completion_code: captures
                    .name("Completion")
                    .expect("no completion")
                    .as_str()
                    .to_owned(),
                label: captures
                    .name("Label")
                    .expect("no label")
                    .as_str()
                    .to_owned(),
            }
        })
        .collect())
}

#[cfg(test)]
mod tests {
    use super::*;
    fn plan_lines(s: &str) -> Vec<PlanLine> {
        line_to_plan_line(construct_plan_lines_from_string(s.to_string())).unwrap()
    }

    mod construct_plan_lines_from_string {
        use super::super::*;
        #[test]
        fn empty_string() {
            assert_eq!(construct_plan_lines_from_string("".to_string()).len(), 0)
        }

        #[test]
        fn one_line() {
            assert_eq!(construct_plan_lines_from_string("red".to_string()).len(), 1)
        }

        #[test]
        fn many_lines() {
            assert_eq!(
                construct_plan_lines_from_string("red\nblue\ngree".to_string()).len(),
                3
            )
        }

        #[test]
        fn middle_empty_line() {
            assert_eq!(
                construct_plan_lines_from_string("red\n\ngree".to_string()).len(),
                2
            )
        }

        #[test]
        fn middle_comment_line() {
            assert_eq!(
                construct_plan_lines_from_string("red\n#blue\ngree".to_string()).len(),
                2
            )
        }
    }

    mod construct_plan_tree {
        use super::super::*;
        use super::plan_lines;

        fn check_step(step: &Box<Step>, label: &str, complete: bool) {
            assert_eq!(step.label, label);
            assert_eq!(step.completion_code, if complete { "X" } else { " " });
        }

        #[test]
        fn simple_plan() {
            let plan_lines = plan_lines("[ ] 1\n  [ ] 2\n  [ ] 3");

            let plan = construct_plan_tree(&plan_lines[0], &plan_lines[1..]);

            check_step(&plan, "1", false);
            check_step(&plan.children[0], "2", false);
            check_step(&plan.children[1], "3", false);
        }

        #[test]
        fn deeper_plan() {
            let plan_lines = plan_lines("[ ] 1\n  [ ] 2\n    [ ] 2.1\n    [X] 2.2\n  [ ] 3");

            let plan = construct_plan_tree(&plan_lines[0], &plan_lines[1..]);

            check_step(&plan, "1", false);
            check_step(&plan.children[0], "2", false);
            check_step(&plan.children[0].children[0], "2.1", false);
            check_step(&plan.children[0].children[1], "2.2", true);
            check_step(&plan.children[1], "3", false);
        }
    }
}
