use crate::plan::Step;
use crate::storage;
use git2::Repository;

pub fn status(repo: Repository, verbosity: u32) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::read_storage(workdir).and_then(|storage| {
        if storage.current_plan_slug == "" {
            println!("I don't think we have a plan right now. Why not start one?");
            return Ok(());
        }
        let mut plan = Step::load(workdir, &storage.current_plan_slug, verbosity)
            .expect("unreadable current plan");
        let current_step = plan.best_next_step();

        println!(
            "Our currently active plan is:\n  {}\n\nI think we're on this step:\n  {}",
            storage
                .branches_to_plans
                .get(&storage.current_plan_slug)
                .unwrap(),
            current_step.label
        );
        Ok(())
    })
}
