use crate::storage;
use git2::Repository;
use skim::prelude::*;
use std::io::Cursor;
pub fn drop(repo: Repository) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::read_storage(workdir).and_then(|storage| {
        let options = SkimOptionsBuilder::default()
            .height(Some("50%"))
            .build()
            .unwrap();
        let item_reader = SkimItemReader::default();

        let input = storage
            .branches_to_plans
            .iter()
            .collect::<Vec<(&String, &String)>>();

        let items = item_reader.of_bufread(Cursor::new(
            input
                .iter()
                .map(|i| i.1.clone())
                .collect::<Vec<String>>()
                .join("\n"),
        ));

        let selected_items = Skim::run_with(&options, Some(items))
            .map(|out| out.selected_items)
            .unwrap_or_else(|| Vec::new());

        selected_items
            .get(0)
            .and_then(|i| {
                input
                    .iter()
                    .find(|(_, label)| **label == i.text().to_string())
            })
            .and_then(|slug| {
                storage::read_storage(workdir)
                    .and_then(|mut storage| {
                        if *slug.0 == storage.current_plan_slug {
                            storage.current_plan_slug = "".to_string();
                        }
                        storage.branches_to_plans.remove(slug.0);
                        storage::write_storage(workdir, storage)
                    })
                    .and_then(|s| {
                        println!("The plan has been forgotten.");
                        Ok(s)
                    })
                    .ok()
            });
        Ok(())
    })
}
