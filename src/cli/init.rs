use crate::storage;
use std::collections::HashMap;
use std::path;

pub fn init<P: AsRef<path::Path>>(git_root: P) -> Result<(), storage::StorageError> {
    println!("setting up your planning environment...");
    storage::create_storage(&git_root).expect("could not create the storage folder!");
    match storage::write_storage(
        &git_root,
        storage::StorageFormat {
            current_plan_slug: "".to_owned(),
            branches_to_plans: HashMap::new(),
            file_format_version: 1,
        },
    ) {
        Ok(_) => {}
        Err(e) => {
            panic!(e);
        }
    }

    println!("we're all set---happy planning!");
    Ok(())
}
