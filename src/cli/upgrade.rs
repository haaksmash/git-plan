use crate::storage;
use std::collections::HashMap;
use std::fs;
use std::path;

pub fn upgrade<P: AsRef<path::Path>>(git_root: P) -> Result<(), storage::StorageError> {
    storage::read_storage(&git_root)
        .and_then(|_| {
            println!("we're all set!");
            Ok(())
        })
        .or_else(|_| {
            println!("upgrading storage format to 1...");
            // before 1, the CURRENT file looked like this:
            //   plan-slug-here:0|0|2
            let plan_directory = git_root.as_ref().join(".git-plans");
            let current_file_contents = fs::read_to_string(plan_directory.join("CURRENT"))
                .expect("no CURRENT file in planning directory");
            let components: Vec<&str> = current_file_contents.split(":").collect();
            storage::write_storage(
                &git_root,
                storage::StorageFormat {
                    current_plan_slug: components[0].to_owned(),
                    branches_to_plans: HashMap::new(),
                    file_format_version: 1,
                },
            )
            .and(upgrade(git_root))
        })
}
