mod drop;
mod init;
mod list;
mod new;
mod next;
mod refine;
mod status;
mod switch;
mod upgrade;
pub use drop::drop;
pub use init::init;
pub use list::list;
pub use new::new;
pub use next::next;
pub use refine::refine;
pub use status::status;
use structopt::StructOpt;
pub use switch::switch_by_label;
pub use upgrade::upgrade;

#[derive(Debug, StructOpt)]
/// git-plan is a git plugin that bundles in task-list tracking to your work.
pub struct Plan {
    #[structopt(short, parse(from_occurrences), long)]
    pub verbosity: u32,

    #[structopt(subcommand)]
    pub cmd: Option<Command>,
}

#[derive(Debug, StructOpt)]
pub enum Command {
    /// Sets up the planner. run this first!
    Init {},

    /// Updates the planner to the latest version
    ///
    /// Should be quick, but no promises!
    Upgrade {},

    /// Creates and stores a new plan in the planner
    New {
        #[structopt(long, help = "Do not create a branch for this plan")]
        no_branch: bool,
        #[structopt(long, help = "Do not switch to the branch for this plan")]
        no_switch: bool,

        goal: String,
    },

    /// Opens $EDITOR so that you can make changes to the current plan
    ///
    /// refine allows you to make changes to the current plan.
    /// It will open $EDITOR with the plan loaded in; when $EDITOR exits, the planner recomputes
    /// the best step to be working on based on the latest plan.
    Refine {},

    /// Checks off the current step
    ///
    /// It will mark the current step as completed in the plan file, and update the current step to
    /// the next one that it can find.  If there are no more steps, the command will make that
    /// clear.
    ///
    /// By default, the command will create a commit from your current index---so make sure that
    /// you've staged the appropriate changes.
    Next {
        #[structopt(long, help = "Do not create a commit for this step")]
        no_commit: bool,
    },

    /// Lists all in-progress plans
    ///
    /// Shows all plans registerd in the current repository, and indicates which one, if any,
    /// you're currently working on.
    List {},

    /// Switches between plans
    Switch {},

    /// Drops a plan from the assistant's memory
    ///
    /// E.g., if you're done with the plan.
    Drop {},
}
