use crate::storage;
use git2::Repository;
use skim::prelude::*;
use std::io::Cursor;

pub fn switch_by_label(repo: Repository) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::read_storage(workdir).and_then(|storage| {
        let options = SkimOptionsBuilder::default()
            .height(Some("50%"))
            .build()
            .unwrap();
        let item_reader = SkimItemReader::default();

        let input = storage
            .branches_to_plans
            .iter()
            .collect::<Vec<(&String, &String)>>();

        let items = item_reader.of_bufread(Cursor::new(
            input
                .iter()
                .map(|i| i.1.clone())
                .collect::<Vec<String>>()
                .join("\n"),
        ));

        let selected_items = Skim::run_with(&options, Some(items))
            .map(|out| out.selected_items)
            .unwrap_or_else(|| Vec::new());

        selected_items
            .get(0)
            .and_then(|i| {
                input
                    .iter()
                    .find(|(_, label)| **label == i.text().to_string())
            })
            .and_then(|slug| {
                let branch_ref = repo.resolve_reference_from_short_name(slug.0).unwrap();
                repo.set_head(branch_ref.name().unwrap()).ok()
            })
            .and_then(|success| {
                println!("You're the boss; switching our focus (and branch)... now.");
                Some(success)
            });
        Ok(())
    })
}
