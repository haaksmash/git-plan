use crate::plan::Step;
use crate::storage;
use git2::Repository;
use regex::Regex;
use std::env;
use std::fs;

pub fn new(
    repo: Repository,
    verbosity: u32,
    goal: String,
    create_branch: bool,
    switch_branch: bool,
) -> Result<(), storage::StorageError> {
    let slug = make_slug(&goal);
    let workdir = repo.workdir().expect("not in a git repository");
    if verbosity > 3 {
        println!("derived new branch slug: {:?}", slug);
    }

    if create_branch {
        match repo.branch(
            &slug,
            &repo.head().unwrap().peel_to_commit().unwrap(),
            false,
        ) {
            Err(e) => {
                println!("I tried to create a branch called {:?}, but I couldn't. Maybe the branch already exists?", slug);
                println!(
                    "If you still want to start this plan, try using the --no-branch argument."
                );
                if verbosity > 3 {
                    println!("{}", e);
                }
            }
            Ok(_) => {
                println!(
                    "I've created a git branch for us to keep this plan organized:\n  {}",
                    slug
                );
            }
        }
    }

    if switch_branch {
        let branch_ref = repo.resolve_reference_from_short_name(&slug).unwrap();
        match repo.set_head(branch_ref.name().unwrap()) {
            Err(e) => {
                println!("I tried to switch us to that branch, but something stopped me.");
                if verbosity > 3 {
                    println!("{}", e);
                }
            }
            Ok(_) => {
                println!("I've gone ahead and switched us to that branch.");
            }
        }
    }

    // create a task file

    let file_name = storage::create_plan_file(workdir, &slug);
    if verbosity > 3 {
        println!("creating plan file at {:?}", file_name);
    }
    if !file_name.exists() {
        match fs::write(
            &file_name,
            format!(
                concat!(
                    "# git-plan will ignore any lines that start with a #, like this one.\n",
                    "# git-plan parses (and modifies!) this file to understand how\n",
                    "# it should understand future commands.\n",
                    "[ ] {}\n",
                    "  [ ] <your first subtask goes here!>"
                ),
                goal
            ),
        ) {
            Err(e) => {
                println!("I couldn't create the new task file for you!");
                if verbosity > 3 {
                    println!("{}", e);
                }
            }
            Ok(_) => {
                // open editor to that temp file
                // save file to plans directory
            }
        }
    } else {
        if verbosity > 3 {
            println!("plan file detected; not overwriting");
        }
        println!("Let's take a look at the existing plan file...");
        println!("");
        println!("  <your $EDITOR should have opened at this point!>");
        println!("");
    }

    let editor = env::var("EDITOR").expect("no $EDITOR");
    std::process::Command::new(editor)
        .arg(&file_name)
        .status()
        .expect("$EDITOR command failed");

    storage::read_storage(workdir).and_then(|mut storage| {
        // figure out the best next step for the new plan
        let next_step = Step::load(workdir, &slug, verbosity)
            .map(|mut plan| plan.best_next_step().clone())
            .expect("should have a next step here");
        storage.branches_to_plans.insert(slug.to_owned(), goal);
        storage.current_plan_slug = slug;
        storage::write_storage(workdir, storage).unwrap();
        println!(
            "We have a plan. I think this is a good starting point:\n  {}",
            next_step.label
        );
        Ok(())
    })
}

fn make_slug(string: &String) -> String {
    let r = Regex::new(r"[-\s!.?`@\$%^&*()={}\[\]|\\~/_]+").unwrap();
    r.replace_all(&string, "-").to_string().to_lowercase()
}
