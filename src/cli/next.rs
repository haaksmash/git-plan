use crate::plan::{Step, StepPrintFormat};
use crate::storage;
use git2::Repository;

pub fn next(
    repo: Repository,
    verbosity: u32,
    make_commit: bool,
) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::current_plan_slug(workdir, verbosity).map(|slug| {
        let mut plan = Step::load(workdir, &slug, verbosity).expect("unreadable current plan");
        if verbosity > 3 {
            println!(
                "old plan specification:\n{}",
                plan.to_string(StepPrintFormat::Pretty)
            );
        }
        let mut next_step = plan.best_next_step();
        next_step.completion_code = "COMPLETED NOW".to_owned();

        if make_commit {
            let id = repo.index().unwrap().write_tree().unwrap();
            let commit = repo
                .commit(
                    Some("HEAD"),
                    &repo.signature().unwrap(),
                    &repo.signature().unwrap(),
                    &next_step.label,
                    &repo.find_tree(id).unwrap(),
                    &[&(repo
                        .find_commit(repo.refname_to_id("HEAD").unwrap())
                        .unwrap())],
                )
                .unwrap();
            println!(
                "Great work; I've committed our progress in {:.8}.\n\nOur next step is:\n  {}",
                commit,
                plan.best_next_step().label
            );
        } else {
            println!(
                "Great work. Our next step is:\n  {}",
                plan.best_next_step().label
            );
        }

        if verbosity > 3 {
            println!(
                "new plan specification:\n{}",
                plan.to_string(StepPrintFormat::Pretty)
            );
        }

        plan.store(workdir, &slug).unwrap();
    })
}
