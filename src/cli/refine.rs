use crate::plan::Step;
use crate::storage;
use git2::Repository;
use std::env;

pub fn refine(repo: Repository, verbosity: u32) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::current_plan_slug(workdir, verbosity).map(|slug| {
        let file_name = storage::create_plan_file(workdir, &slug);

        let editor = env::var("EDITOR").expect("no $EDITOR");
        std::process::Command::new(editor)
            .arg(&file_name)
            .status()
            .expect("$EDITOR command failed");

        let next_step = Step::load(workdir, &slug, verbosity)
            .map(|mut plan| plan.best_next_step().clone())
            .expect("should have a next step here");
        println!(
            "Got it; based on your changes, looks like we should be focusing on this:\n {}",
            next_step.label
        );
    })
}
