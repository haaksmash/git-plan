use crate::storage;
use git2::Repository;

pub fn list(repo: Repository) -> Result<(), storage::StorageError> {
    let workdir = repo.workdir().expect("not in a git repository");
    storage::read_storage(workdir).and_then(|storage| {
        println!("Here's all the plans I know about:\n");
        for (slug, label) in storage.branches_to_plans.iter() {
            println!(
                "{}| {}",
                if slug == &storage.current_plan_slug {
                    "*"
                } else {
                    " "
                },
                label,
            );
        }
        println!("\nA * indicates our current plan");
        Ok(())
    })
}
