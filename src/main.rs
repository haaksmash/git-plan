mod cli;
mod plan;
mod storage;
use git2::Repository;
use std::env;
use std::fs;
use std::path;
use structopt::StructOpt;

fn main() {
    let command = cli::Plan::from_args();

    if command.verbosity > 3 {
        println!("running with high verbosity");
    }

    let current_dir = env::current_dir().expect("git-plan could not read your current directory!");
    let git_root =
        find_git_directory(current_dir).expect("git-plan could not find the .git directory!");
    let verbosity = command.verbosity;

    command
        .cmd
        .and_then(|cmd| {
            match cmd {
                cli::Command::Init {} => cli::init(&git_root),

                cli::Command::Upgrade {} => cli::upgrade(&git_root),

                cli::Command::New {
                    goal,
                    no_branch,
                    no_switch,
                } => cli::new(
                    Repository::open(&git_root).unwrap(),
                    verbosity,
                    goal,
                    !no_branch,
                    !no_switch,
                ),

                cli::Command::Refine {} => {
                    cli::refine(Repository::open(&git_root).unwrap(), verbosity)
                }

                cli::Command::Next { no_commit } => {
                    cli::next(Repository::open(&git_root).unwrap(), verbosity, !no_commit)
                }

                cli::Command::List {} => cli::list(Repository::open(&git_root).unwrap()),

                cli::Command::Switch {} => {
                    cli::switch_by_label(Repository::open(&git_root).unwrap())
                }

                cli::Command::Drop {} => cli::drop(Repository::open(&git_root).unwrap()),
            }
            .ok()
        })
        .or_else(|| cli::status(Repository::open(&git_root).unwrap(), verbosity).ok());
}

fn find_git_directory<P: AsRef<path::Path>>(path: P) -> Result<path::PathBuf, &'static str> {
    match fs::read_dir(path.as_ref().join(".git")) {
        Ok(_) => {
            return Ok(path.as_ref().to_path_buf());
        }
        Err(_) => match path.as_ref().parent() {
            Some(p) => {
                return find_git_directory(p);
            }
            None => {
                return Err("couldn't find the .git directory");
            }
        },
    }
}
