use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error;
use std::fmt;
use std::fs;
use std::path;

const PLANS_FOLDER: &str = ".git-plans";
const STORAGE_FILE_NAME: &str = "CURRENT";

#[derive(Debug)]
pub enum StorageError {
    BadJson,
    NoStorageFile,
}

impl fmt::Display for StorageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            StorageError::BadJson => write!(f, "storage file is not valid json"),
            // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
            StorageError::NoStorageFile => write!(f, "no storage file"),
        }
    }
}

impl error::Error for StorageError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StorageFormat {
    pub current_plan_slug: String,
    pub branches_to_plans: HashMap<String, String>,
    pub file_format_version: u64,
}

pub fn create_storage<P: AsRef<path::Path>>(git_root: P) -> Result<(), std::io::Error> {
    let plan_directory = git_root.as_ref().join(PLANS_FOLDER);
    fs::create_dir_all(plan_directory.join("plans"))
}

pub fn create_plan_file(workdir: &path::Path, slug: &String) -> path::PathBuf {
    workdir
        .join(PLANS_FOLDER)
        .join("plans")
        .join(format!("{}.txt", slug))
}

pub fn read_storage<P: AsRef<path::Path>>(git_root: P) -> Result<StorageFormat, StorageError> {
    let plan_directory = git_root.as_ref().join(PLANS_FOLDER);
    fs::read_to_string(plan_directory.join(STORAGE_FILE_NAME))
        .or(Err(StorageError::NoStorageFile))
        .and_then(|current_file_contents| {
            serde_json::from_str(&current_file_contents).or(Err(StorageError::BadJson))
        })
        .or_else(|e| {
            println!("I couldn't read our storage file; did you run `git plan init`?");
            Err(e)
        })
}

pub fn write_storage<P: AsRef<path::Path>>(
    git_root: P,
    storage: StorageFormat,
) -> Result<(), StorageError> {
    let plan_directory = git_root.as_ref().join(PLANS_FOLDER);
    serde_json::to_string(&storage)
        .or(Err(StorageError::BadJson))
        .and_then(|json_string| {
            fs::write(plan_directory.join(STORAGE_FILE_NAME), json_string)
                .or(Err(StorageError::NoStorageFile))
        })
}

pub fn current_plan_slug<P: AsRef<path::Path>>(
    workdir: P,
    verbosity: u32,
) -> Result<String, StorageError> {
    match read_storage(workdir) {
        Err(e) => {
            println!("I couldn't understand our storage file!");
            if verbosity > 3 {
                println!("{:?}", e);
            }

            Err(e)
        }

        Ok(storage) => Ok(storage.current_plan_slug),
    }
}

pub fn read_plan_file<P: AsRef<path::Path>>(
    git_root: P,
    slug: &str,
) -> Result<String, std::io::Error> {
    let plan_directory = git_root.as_ref().join(PLANS_FOLDER).join("plans");
    fs::read_to_string(plan_directory.join(format!("{}.txt", slug)))
}

pub fn write_plan_file<P: AsRef<path::Path>>(
    git_root: P,
    slug: &str,
    bytes: String,
) -> Result<(), std::io::Error> {
    let plan_directory = git_root.as_ref().join(PLANS_FOLDER).join("plans");
    fs::write(plan_directory.join(format!("{}.txt", slug)), bytes)
}
