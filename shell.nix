with import <nixpkgs> {};

pkgs.mkShell {
  buildInputs = [
    rustc
    cargo
    rustfmt
    openssl
    zlib
  ];
}
